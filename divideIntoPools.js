(function() {
    'use strict';

    var MIN_AMOUNT = 0.01;

    function add(a, b) {
      return a + b;
    }

    function calcPool(total, p) {
      return total * p;
    }

    function round10(n) {
      return Math.round(n * 100) / 100;
    }

    function divideIntoPools(amount, percentage) {
      var eachReminder, greaterIdx;

      amount = parseFloat(amount);
      if(!amount || !percentage) {
        throw new TypeError('Money should be a number and percentage an array.');
      }

      if(percentage.reduce(add) !== 1) {
        throw new RangeError('The sum of percentages must be 100%.');
      }

      if(amount < MIN_AMOUNT) {
        throw new RangeError('Money should be at least one penny.');
      }

      var pools = percentage.map(function(p, idx) {
        return round10(calcPool(amount, p));
      });

      eachReminder = (amount - pools.reduce(add)) / (percentage.length - 1);
      greaterIdx = pools.indexOf(Math.max.apply(Math, pools));

      return pools.map(function(p, idx) {
        if(idx === greaterIdx) {
          return p;
        } else {
          return round10(p + eachReminder);
        }
      });
    }

    window.divideIntoPools = divideIntoPools;
})();
